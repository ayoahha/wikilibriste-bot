#!/bin/bash

set -e

VENV_DIR="venv"
REQUIREMENTS_FILE="requirements.txt"
TEMP_REQUIREMENTS="requirements.temp.txt"

function check_venv() {
    if [ -z "${VIRTUAL_ENV}" ]; then
        echo "Virtual environment not activated. Activating..."
        if [ ! -d "$VENV_DIR" ]; then
            python3 -m venv "$VENV_DIR"
        fi
        source "$VENV_DIR/bin/activate"
    fi
}

function update_deps() {
    echo "Updating all dependencies to their latest compatible versions..."
    pip install --upgrade pip
    
    # Get list of outdated packages and update them
    pip list --outdated --format=json | python3 -c "
import json, sys
packages = json.load(sys.stdin)
for package in packages:
    print(package['name'])
" | xargs -r pip install --upgrade

    # Update requirements.txt with new versions
    pip freeze > "$TEMP_REQUIREMENTS"
    
    # Keep only direct dependencies (those in original requirements.txt)
    if [ -f "$REQUIREMENTS_FILE" ]; then
        echo "Updating requirements.txt with new versions..."
        while IFS= read -r pkg; do
            pkg_name=$(echo "$pkg" | cut -d'=' -f1)
            new_version=$(grep "^$pkg_name==" "$TEMP_REQUIREMENTS" || echo "")
            if [ ! -z "$new_version" ]; then
                sed -i "s/^$pkg_name.*/$new_version/" "$REQUIREMENTS_FILE"
            fi
        done < "$REQUIREMENTS_FILE"
    else
        mv "$TEMP_REQUIREMENTS" "$REQUIREMENTS_FILE"
    fi
    rm -f "$TEMP_REQUIREMENTS"
}

function check_outdated() {
    echo "Checking for outdated packages..."
    pip list --outdated
}

function install_deps() {
    echo "Installing dependencies from requirements.txt..."
    pip install -r "$REQUIREMENTS_FILE"
    pip install -e .
}

case "$1" in
    "update")
        check_venv
        update_deps
        ;;
    "check")
        check_venv
        check_outdated
        ;;
    "install")
        check_venv
        install_deps
        ;;
    *)
        echo "Usage: $0 {update|check|install}"
        echo "  update  - Update all dependencies to their latest versions"
        echo "  check   - Check for outdated packages"
        echo "  install - Install dependencies from requirements.txt"
        exit 1
        ;;
esac 
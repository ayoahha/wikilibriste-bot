# Déploiement et Gestion des Dépendances

Ce document décrit le processus de déploiement et la gestion des dépendances pour le bot Wikilibriste.

## Workflow de Développement

1. **Création d'une Issue**
   - Créer une issue avec un titre clair (description-courte)
   - Décrire de façon claire et concise la mise à jour attendue
   - Utiliser les labels appropriés

2. **Création d'une Merge Request (MR)**
   - Créer une branche depuis `develop`
   - Lier la MR à l'issue correspondante, ou créer cette MR depuis l'issue
   - Vérifier que la MR suit la convention de nommage : `<numéro-issue>-description-courte`

3. **Développement**
   - Implémenter les changements
   - S'assurer que les tests passent
   - Suivre les conventions de commit ([conventional commits](https://www.conventionalcommits.org/))

4. **Merge dans develop**
   - Vérifier que la CI passe après push
   - Review du code si nécessaire par un pair
   - Merge dans `develop`
   - Vérifier que la CI passe après le merge

Seul l'administrateur du projet peut merge dans `main`.

5. **Déploiement vers main**
   - Créer une MR de `develop` vers `main` intitulée "Bump version"
   - La CI exécute automatiquement :
     - Tests
     - Versioning
     - Déploiement sur le serveur qui héberge le bot

## Gestion Automatisée des Dépendances

### Vérifications Automatiques

Le système effectue des vérifications automatiques des dépendances :

1. **Sur develop**
   - À chaque push
   - Génère un rapport de dépendances obsolètes
   - Visible dans les artifacts de la CI

2. **Vérifications Planifiées**
   - Exécutées hebdomadairement
   - Utilise Renovate pour créer des MR automatiques
   - Fusionne automatiquement les mises à jour mineures/correctifs

### Commandes Manuelles

Le script `scripts/manage_deps.sh` fournit les trois commandes suivantes :

```bash
# Vérifier les dépendances obsolètes
./scripts/manage_deps.sh check

# Mettre à jour toutes les dépendances
./scripts/manage_deps.sh update

# Installer les dépendances
./scripts/manage_deps.sh install
```

### Processus de Déploiement

Le déploiement est automatisé via GitLab CI et inclut :

1. **Vérifications Préalables**
   - Tests unitaires
   - Analyse statique du code
   - Vérification des dépendances

2. **Versioning**
   - Génération automatique du numéro de version
   - Création des tags Git
   - Mise à jour du CHANGELOG

3. **Déploiement**
   - Mise à jour du code sur le server qui héberge le bot
   - Mise à jour des dépendances
   - Redémarrage automatique du bot

4. **Sécurité**
   - Sauvegarde des dépendances avant mise à jour
   - Rollback automatique en cas d'échec
   - Retry automatique en cas d'erreur système

### Gestion des Erreurs

En cas d'échec du déploiement :

1. **Erreurs de Dépendances**
   - Rollback automatique vers la dernière version fonctionnelle
   - Les logs sont disponibles dans `~/.config/wkbot/bot.log`

2. **Erreurs Système**
   - Retry automatique (max 2 tentatives)
   - Notification dans la CI directement

3. **Erreurs Manuelles**
   - Utiliser `./scripts/manage_deps.sh install` pour revenir à un état stable
   - Vérifier les logs pour diagnostiquer le problème

## Maintenance

### Backups

Les sauvegardes sont gérées automatiquement :
- Sauvegarde des dépendances avant chaque mise à jour
- Conservation des 5 dernières sauvegardes
- Stockées dans `~/.config/wkbot/backups/`

### Monitoring

Monitoring et troubleshooting du bot :
```bash
# Vérifier le statut
ps aux | grep "python -m wkbot.main"

# Consulter les logs
tail -f ~/.config/wkbot/bot.log

# Vérifier les dépendances
./scripts/manage_deps.sh check
``` 
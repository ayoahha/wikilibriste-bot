#!/bin/bash

# Check if branch and commit hash are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <branch> <commit_hash>"
    exit 1
fi

BRANCH=$1
COMMIT_HASH=$2
APP_DIR="/home/ubuntu/wikilibriste-bot"
VENV_DIR="/home/ubuntu/venv"
CONFIG_DIR="/home/ubuntu/.config/wkbot"
BACKUP_DIR="$CONFIG_DIR/backups"

echo "Deploying branch $BRANCH commit $COMMIT_HASH..."

# Create backup directory if it doesn't exist
mkdir -p "$BACKUP_DIR"

# Update repository
if [ ! -d "$APP_DIR" ]; then
    echo "Cloning repository..."
    git clone https://gitlab.com/ayoahha/wikilibriste-bot.git "$APP_DIR"
fi

cd "$APP_DIR" || exit 1
git fetch origin
git checkout "$BRANCH"
git reset --hard "$COMMIT_HASH"

# Backup current virtual environment
if [ -d "$VENV_DIR" ]; then
    echo "Backing up current dependencies..."
    source "$VENV_DIR/bin/activate"
    pip freeze > "$BACKUP_DIR/requirements.backup.$(date +%Y%m%d_%H%M%S).txt"
    deactivate
    mv "$VENV_DIR" "${VENV_DIR}.old"
fi

# Create new virtual environment
echo "Creating fresh virtual environment..."
python3 -m venv "$VENV_DIR"

# Activate virtual environment and install dependencies
source "$VENV_DIR/bin/activate"
echo "Upgrading pip and installing dependencies..."
pip install --upgrade pip
pip install -r requirements.txt
pip install . --no-deps

# Verify installation
if [ $? -eq 0 ]; then
    echo "Dependencies installed successfully!"
    rm -rf "${VENV_DIR}.old"
else
    echo "Error installing dependencies! Rolling back..."
    deactivate
    rm -rf "$VENV_DIR"
    mv "${VENV_DIR}.old" "$VENV_DIR"
    source "$VENV_DIR/bin/activate"
    exit 1
fi

# Ensure config directory exists
mkdir -p "$CONFIG_DIR"

# Create/update .env file if it doesn't exist
if [ ! -f "$CONFIG_DIR/.env" ]; then
    echo "Please create $CONFIG_DIR/.env with your bot token"
    echo "Example: API_TOKEN=your_bot_token_here"
fi

# Clean old backups (keep last 5)
cd "$BACKUP_DIR" || exit 1
ls -t requirements.backup.*.txt | tail -n +6 | xargs -r rm

echo "Deployment complete!"

#!/bin/bash

# Stop any existing bot process
pkill -f "python -m wkbot.main" || true

# Start the bot in background
cd /home/ubuntu/wikilibriste-bot
source /home/ubuntu/venv/bin/activate
nohup python -m wkbot.main > /home/ubuntu/.config/wkbot/bot.log 2>&1 &

echo "Bot restarted! Check logs at /home/ubuntu/.config/wkbot/bot.log"

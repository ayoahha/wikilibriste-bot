[![Latest Release](https://gitlab.com/ayoahha/wikilibriste-bot/-/badges/release.svg)](https://gitlab.com/ayoahha/wikilibriste-bot/-/releases) 
[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
[![coverage report](https://gitlab.com/ayoahha/wikilibriste-bot/badges/main/coverage.svg)](https://gitlab.com/ayoahha/wikilibriste-bot/-/commits/main)

---

# Bot wikilibriste

Bot d'aide à la navigation du contenu sur [Wikilibriste](https://wikilibriste.fr).

Implémenté en python (3.10+) et déployé sur le [Groupe telegram Wilibriste-Colinu](https://t.me/securite_informatique_libre).

_L'application est générée en package avec setuptools._

## Déploiement

### Prérequis

1. Serveur Ubuntu 24.04
2. Clé SSH
3. Token du Bot Telegram (depuis @BotFather)

### Configuration initiale du serveur

1. Se connecter au serveur via SSH.

2. Mettre à jour le système et installer les dépendances:
```bash
sudo apt update && sudo apt upgrade -y
sudo apt install -y python3-venv python3-pip git
```

3. Créer les répertoires nécessaires:
```bash
mkdir -p ~/.config/wkbot
```

4. Configuration initiale (première installation uniquement):
   - Créer et configurer le fichier .env:
   ```bash
   touch ~/.config/wkbot/.env
   chmod 600 ~/.config/wkbot/.env
   # Ajouter le token du bot dans .env:
   echo "API_TOKEN=<le_token>" > ~/.config/wkbot/.env
   ```
   - Copier les scripts de déploiement:
   ```bash
   scp -i ~/.ssh/wkbot_vps_key deploy/scripts/deploy.sh ubuntu@<SERVER_IP>:~/.config/wkbot/
   scp -i ~/.ssh/wkbot_vps_key deploy/scripts/restart.sh ubuntu@<SERVER_IP>:~/.config/wkbot/
   chmod +x ~/.config/wkbot/*.sh
   ```

### Configuration GitLab CI/CD

Ajouter les variables suivantes dans les paramètres GitLab CI/CD (Settings > CI/CD > Variables):

- `SERVER_IP`: <IP du SERVEUR>
- `SERVER_USER`: <UTILISATEUR>
- `SSH_PRIVATE_KEY`: Contenu de la clé privée SSH
- `GL_TOKEN`: Token d'accès personnel GitLab avec les permissions suivantes:
  - api
  - read_repository
  - write_repository
  
Ce token est nécessaire pour le versioning automatique avec semantic-release.

### Déploiement manuel

Si vous devez déployer manuellement:

1. Se connecter au serveur
2. Lancer le déploiement:
```bash
~/.config/wkbot/deploy.sh main latest_commit_hash
```

3. Démarrer le bot:
```bash
~/.config/wkbot/restart.sh
```

### Surveillance et Vérification

1. Vérifier que le bot est en cours d'exécution:
```bash
ps aux | grep "python -m wkbot.main"
```

2. Consulter les logs pour détecter d'éventuelles erreurs:
```bash
tail -f ~/.config/wkbot/bot.log
```

3. Tester le bot sur Telegram:
   - Envoyer la commande `/start` au bot
   - Vérifier que le bot répond correctement
   - Tester les autres commandes disponibles

### Dépannage

1. Si le bot ne démarre pas, vérifier:
   - Les logs: `~/.config/wkbot/bot.log`
   - Le fichier d'environnement: `~/.config/wkbot/.env`
   - L'environnement virtuel Python: `~/venv`

2. Pour redémarrer le bot:
```bash
~/.config/wkbot/restart.sh
```

3. Pour arrêter le bot:
```bash
pkill -f "python -m wkbot.main"
```

## Configuration

Le bot ne fonctionne qu'avec le token API généré.
Celui-ci n'est pas publié sur le dépôt et n'apparait pas en clair dans l'application.

Afin de le rapatrier :
- Contactez-moi en privé pour initier une communication protégée.

Pour configurer votre environnement :
- Ajoutez un fichier `.env` à ce chemin `~/.config/wkbot`, puis ajoutez ceci dans le fichier :
```bash
API_TOKEN=<le_token>
```

L'application interrogera automatiquement ce fichier lors de son lancement.
Si le fichier n'est pas présent, une erreur sera générée.

## Outils

Le projet utilise les outils suivants :
- la librairie [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) pour l'API telegram.
- la librairie [pylint](https://pylint.readthedocs.io/en/stable/) pour l'analyse de code statique.
- la librairie [pytest](https://docs.pytest.org/en/7.4.x/contents.html) pour les tests unitaires.
- la sémantique [semver](https://semver.org/lang/fr/) pour le versioning.
- la génération des commits [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#specification).

## Gestion des dépendances

Le projet dispose d'un système automatisé de gestion des dépendances. Pour plus de détails, consultez [DEPLOYMENT.md](DEPLOYMENT.md).

### En local

Un script utilitaire `scripts/manage_deps.sh` est disponible avec les commandes suivantes :

```bash
# Vérifier les dépendances obsolètes
./scripts/manage_deps.sh check

# Mettre à jour toutes les dépendances
./scripts/manage_deps.sh update

# Installer les dépendances
./scripts/manage_deps.sh install
```

### Sur le serveur d'hébergement

Les dépendances sont automatiquement mises à jour lors du déploiement avec :
- Sauvegarde des dépendances actuelles
- Mise à jour vers les dernières versions stables
- Retour arrière automatique en cas d'erreur

### Mises à jour automatiques

Le projet utilise Renovate pour la gestion automatique des dépendances :
- Vérification hebdomadaire des mises à jour
- Fusion automatique des mises à jour mineures et correctifs
- Création de MR pour les mises à jour majeures
- Tableau de bord des dépendances disponible dans GitLab

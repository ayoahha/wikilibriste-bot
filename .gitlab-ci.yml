default:
  interruptible: true
stages:
- lint
- test
- version
- deploy
- dependency-update
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
cache:
  paths:
  - ".cache/pip"
  - "venv/"

.python-setup: &python-setup
  image: python:3.12-slim
  before_script:
    - apt-get update -y
    - apt-get install -y git
    - python -m venv venv
    - . venv/bin/activate
    - pip install --upgrade pip
    - pip install -r requirements.txt
    - pip install . --no-deps

dependency-check:
  stage: test
  <<: *python-setup
  script:
    - ./scripts/manage_deps.sh check
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_COMMIT_BRANCH == "develop"
  allow_failure: true
  artifacts:
    reports:
      junit: dependency-report.xml

lint-bot:
  stage: lint
  <<: *python-setup
  script:
    - echo "Lancement de l'analyse du code dans wkbot..."
    - pylint src/wkbot/utils
    - pylint src/wkbot/commands

lint-tests:
  stage: lint
  <<: *python-setup
  script:
    - echo "Lancement de l'analyse du code dans tests..."
    - pylint tests

tests:
  stage: test
  <<: *python-setup
  script:
    - echo "Lancement des tests unitaires..."
    - pytest

versioning:
  stage: version
  image: node:latest
  only:
    refs:
    - main
  script:
  - npm install @semantic-release/gitlab
  - npx semantic-release

dependency-update:
  stage: dependency-update
  image: renovate/renovate:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  variables:
    RENOVATE_PLATFORM: "gitlab"
    RENOVATE_ENDPOINT: "https://gitlab.com/api/v4/"
    RENOVATE_TOKEN: ${GL_TOKEN}
  script:
    - renovate

deploy:
  dependencies:
    - versioning
    - tests
  stage: deploy
  image: debian:latest
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client iputils-ping -y )'
    - eval $(ssh-agent -s)
    - chmod og= "$SSH_PRIVATE_KEY"
    - ssh-add "$SSH_PRIVATE_KEY"
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - echo "Construction et déploiement de l'application..."
    - |
      if ! ssh -i "$SSH_PRIVATE_KEY" "$SERVER_USER"@"$SERVER_IP" "source ~/.config/wkbot/deploy.sh $CI_COMMIT_BRANCH $CI_COMMIT_SHORT_SHA"; then
        echo "Deployment failed! Check logs for details."
        exit 1
      fi
    - echo "Mise à jour des dépendances..."
    - |
      if ! ssh -i "$SSH_PRIVATE_KEY" "$SERVER_USER"@"$SERVER_IP" "cd ~/wikilibriste-bot && source ~/venv/bin/activate && ./scripts/manage_deps.sh update"; then
        echo "Dependency update failed! Rolling back..."
        ssh -i "$SSH_PRIVATE_KEY" "$SERVER_USER"@"$SERVER_IP" "cd ~/wikilibriste-bot && source ~/venv/bin/activate && ./scripts/manage_deps.sh install"
        exit 1
      fi
    - echo "Redémarrage de l'application..."
    - ssh -i "$SSH_PRIVATE_KEY" "$SERVER_USER"@"$SERVER_IP" "~/.config/wkbot/restart.sh"
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure

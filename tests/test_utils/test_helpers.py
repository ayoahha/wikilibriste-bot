"""
Implementation des tests unitaires pour utils.py.

"""
from wkbot.utils.helpers import (
    load_json_file,
    remove_urls,
    get_keywords
)


# TESTS pour la fonction remove_urls().
def test_remove_urls_with_urls():
    """Suppression URLs."""

    message = "Voici un lien vers Google : https://www.google.com"
    result = remove_urls(message)
    assert "https://www.google.com" not in result


def test_remove_urls_without_urls():
    """Pas d'URL dans le message."""

    message = "Ceci est un message sans lien."
    assert remove_urls(message) == message


def test_remove_urls_with_urls_and_text():
    """Suppression URLs dans le texte."""

    message = "Visitez mon site web : http://example.com pour plus d'informations."
    result = remove_urls(message)
    assert "http://example.com" not in result
    assert "Visitez mon site web :" in result


def test_remove_urls_with_urls_and_special_characters():
    """Suppression URLs dans le texte avec caractères spéciaux."""

    message = "Cliquez ici : https://example.com/?param=1&name=John%20Doe"
    result = remove_urls(message)
    assert "https://example.com/?param=1&name=John%20Doe" not in result
    assert "Cliquez ici :" in result


# TESTS pour la fonction get_keywords().
def test_get_keywords_with_matching_subjects():
    """Détection mot-clé avec un modèle qui match."""

    for subject, _ in load_json_file("keywords.json").items():
        message = f"Message contenant un {subject}."

        keywords, _ = get_keywords(message)
        expected_keywords = [subject]
        assert keywords == expected_keywords


def test_get_keywords_with_no_matching_subjects():
    """Détection mot-clé avec un modèle qui ne match pas."""

    # Message sans correspondance avec DB
    for subject, _ in load_json_file("keywords.json").items():
        message = "Ce message ne contient pas de mot-clé."

        keywords, _ = get_keywords(message)
        expected_keywords = [subject]
        assert keywords != expected_keywords


def test_get_keywords_with_invalid_pattern():
    """Détection mot-clé avec modèle qui match, mais qui est invalide."""

    # Message avec un modèle invalide (vpene et non VPN)
    for subject, _ in load_json_file("keywords.json").items():
        message = "Comment que c'est un vpene."

        keywords, _ = get_keywords(message)
        expected_keywords = [subject]
        assert keywords != expected_keywords

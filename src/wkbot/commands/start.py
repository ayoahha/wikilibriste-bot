"""
Fichier module pour la commande START.
Permet de vérifier que le bot est en ligne.

"""
import logging

from telegram import Update
from telegram.ext import ContextTypes

from ..utils.constants import ALLOWED_CHAT_IDS

logger = logging.getLogger(__name__)


async def start_cmd(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    """Ping du bot"""

    message = update.effective_message
    chat_id = update.effective_chat.id

    if message.from_user:
        user_id = message.from_user.id
        user_name = message.from_user.username
        logger.info(
            "/start cmd lancée dans le chat id: %s by %s - %s",
            chat_id,
            user_id,
            user_name
        )
    else:
        logger.info(
            "/start cmd lancée dans le chat id: %s by %s - %s",
            chat_id,
            "ID inconnu",
            "Username inconnu!"
        )
        logger.debug(update)

    if chat_id in ALLOWED_CHAT_IDS:
        await message.reply_text(
            "✅ Bot initialisé et à l'écoute..."
        )
    else:
        await message.reply_text(
            "🛑 Commande autorisée uniquement dans le canal d'administration du bot!"
        )

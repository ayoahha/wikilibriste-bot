"""
Implementation du bot telegram.

"""
import os
import logging
from dotenv import load_dotenv

from telegram import (
    BotCommandScopeAllGroupChats,
    Update
)
from telegram.constants import ParseMode
from telegram.ext import (
    Application,
    ApplicationBuilder,
    CommandHandler,
    MessageHandler,
    filters,
    Defaults,
    JobQueue
)

from .commands.start import start_cmd
from .commands.rules import rules_cmd
from .commands.wiki import wiki_cmd
from .commands.gafam import gafam_cmd
from .commands.don import don_cmd
from .commands.crypt import crypt_cmd
from .commands.file import file_cmd
from .utils.logger import error_handler
from .utils.helpers import (
    raise_app_handler_stop,
    leave_chat,
    build_command_list
)
from .utils.constants import (
    LOGS_CHAT_ID,
    ALLOWED_USERNAMES,
    ALLOWED_CHAT_IDS
)

if os.environ.get("WKBOT_DEBUG"):
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
    )
else:
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
    )
logging.getLogger("apscheduler").setLevel(logging.WARNING)
logging.getLogger("gql").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)


async def post_init(application: Application) -> None:
    """Déclare les commandes utilisables."""

    bot = application.bot
    await bot.set_my_commands(
        build_command_list(),
        scope=BotCommandScopeAllGroupChats(),
    )

def main() -> None:
    """Fonction principale."""

    # Charge les variables d'environnement à partir du fichier .env
    env_path = os.path.expanduser("~/.config/wkbot/.env")
    load_dotenv(dotenv_path=env_path)
    # Récupère la valeur de la variable d'environnement API_TOKEN
    api_token = os.getenv("API_TOKEN")

    # Lance le constructeur du bot
    defaults = Defaults(parse_mode=ParseMode.HTML, disable_web_page_preview=True)
    app = (
        ApplicationBuilder()
        .token(api_token)
        .defaults(defaults)
        .post_init(post_init)
        .job_queue(JobQueue())
        .build()
    )

    # Ne pas prendre en compte les messages dans le canal Logs
    app.add_handler(
        MessageHandler(filters.Chat(chat_id=LOGS_CHAT_ID), raise_app_handler_stop)
    )
    # Quitte les groupes qui ne sont pas autorisés pour intégrer Wkbot
    app.add_handler(
        MessageHandler(
            filters.ChatType.GROUPS
            & ~filters.StatusUpdate.LEFT_CHAT_MEMBER
            & ~(filters.Chat(username=ALLOWED_USERNAMES) | filters.Chat(chat_id=ALLOWED_CHAT_IDS)),
            leave_chat,
        )
    )

    # Commandes du bot
    app.add_handler(CommandHandler("start", start_cmd))
    app.add_handler(CommandHandler("rules", rules_cmd))
    app.add_handler(CommandHandler("wiki", wiki_cmd))
    app.add_handler(CommandHandler("gafam", gafam_cmd))
    app.add_handler(CommandHandler("don", don_cmd))
    app.add_handler(CommandHandler("crypt", crypt_cmd))
    app.add_handler(CommandHandler("file", file_cmd))

    # Handler des erreurs
    app.add_error_handler(error_handler)

    app.run_polling(allowed_updates=Update.ALL_TYPES, close_loop=False)


if __name__ == '__main__':
    main()

"""
Fichier pour la remontée des erreurs du bot.

"""
import html
import json
import logging
import traceback
from typing import cast

from telegram import Update
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from .constants import LOGS_CHAT_ID

logger = logging.getLogger(__name__)


async def error_handler(update: object, context: CallbackContext) -> None:
    """Si des erreurs arrivent, on log et on envoit un message sur le canal des Logs."""

    # On log avant toute choses
    logger.error(msg="Exception while handling an update:", exc_info=context.error)

    # traceback.format_exception returns the usual python message about an exception, but as a
    # list of strings rather than a single string, so we have to join them together.
    # cf. https://github.com/python-telegram-bot/python-telegram-bot/ example = errorhandlerbot.py
    tb_list = traceback.format_exception(
        None, context.error, cast(Exception, context.error).__traceback__
    )
    tb_string = "".join(tb_list)

    update_str = update.to_dict() if isinstance(update, Update) else str(update)
    message_1 = (
        f"An exception was raised while handling an update\n\n"
        f"<pre>update = {html.escape(json.dumps(update_str, indent=2, ensure_ascii=False))}</pre>"
    )
    message_2 = f"<pre>{html.escape(tb_string)}</pre>"

    # On envoie en 2 parties pour éviter de surcharger le message (risque de problèmes d'affichage)
    try:
        sent_message = await context.bot.send_message(
            chat_id=LOGS_CHAT_ID, text=message_1
        )
        await sent_message.reply_html(message_2)
    except BadRequest as exc:
        if "too long" in str(exc):
            message = (
                f"Hey.\nThe error <code>{html.escape(str(context.error))}</code> happened."
                f" The traceback is too long to send, but it was written to the log."
            )
            await context.bot.send_message(chat_id=LOGS_CHAT_ID, text=message)
        else:
            raise exc

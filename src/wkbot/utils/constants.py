"""
Constantes.

"""
ARROW_CHARACTER = "➜"
GROUPE_USERNAME = "securite_informatique_libre"
GROUPE_DEV_USERNAME = "wkbot_dev"
GROUPE_CHAT_ID = "@" + GROUPE_USERNAME
DEV_CHAT_ID = -1002124940108
LOGS_CHAT_ID = -1001907596326
BOT_CHANNEL_CHAT_ID = 1464722139
ALLOWED_USERNAMES = (GROUPE_USERNAME, GROUPE_DEV_USERNAME)
ALLOWED_CHAT_IDS = {
    BOT_CHANNEL_CHAT_ID,    # wikilibristeBot chat
    LOGS_CHAT_ID,           # logs chat
}
SELF_BOT_NAME = "wikilibristBot"
WIKI_URL = "https://wikilibriste.fr"
RATE_LIMIT_SPACING = 4
PATTERNS_URL = (
    r'\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]'
    r'{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s('
    r')<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+'
    r'\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))')

# Messages pour /wiki
MSG_NO_KEYWORDS = """🤖 Aucun Mot-clé détecté 🤖
N'hésitez pas à vérifier dans <a href="https://wikilibriste.fr">le wiki</a>.
"""
MSG_FIRST = """🤖 Mots-clés détectés 🤖
"""
MSG_SECOND = """

💡 Avez-vous jeté un oeil sur ces articles du wiki :
"""
APK = """\n\n🛑 Concernant les apk et plus généralement tous les fichiers 'exécutables' : nous
recommandons d'éviter de télécharger ce type de fichiers (apk, exe, msi...)
en dehors de magasins d'applications.

En effet, le téléchargement de fichiers sur des sites tiers (type apkpure, etc.) ou d'applications (whatsapp, telegram, etc.)
n'est pas une bonne pratique et peut mener au téléchargement d'un malware !
"""
MSG_NO_REPLY_MSG = """
🤖 Aucun message lié avec la commande 🤖
Merci de répondre à un message du groupe pour que je puisse l'analyser.
"""

# Messages pour les règles du groupe
GROUPE_RULES = """
✅ <b>Veuillez respecter le thème du groupe</b> :
- Les licences libres (logiciels, matériels, outils...).
- La sécurité, résilience, hygiène numérique avec des outils libres ou résilients, éthiques, respectueux de la vie privée...
- Les réseaux décentralisés, fédérés, interopérables et la neutralité du net.

⚠️ <b>Règles importantes</b> :
- Nous ne sommes pas un groupe de "Nouvelles Sociétales", mais de Nouvelles Techniques, liées au numérique.
- Merci de ne pas poster de nouvelles anxiogènes et/ou avec un fort accent politique. D'autres groupes existent pour cela.
- Merci de ne pas poster de Liens sans description.
- Merci de ne pas partager de fichiers sur le groupe (.apk, .exe, .jar, .zip, .csv, .xlsx...)
👉 Il est préférable d'envoyer le lien vers le dépôt ou le code source directement.
👉 Il est recommandé de télécharger les fichiers APK sur F-droid (ou équivalent) ou Aurora store.
Et pour nos malheureux membres utilisant toujours Windows : de façon générale, toujours faire attention aux .exe ou .msi provenant de sources hors de l'éditeur de logiciel (bien souvent source de malware pour vol de données ou pire) !

🛑 Nous nous réservons le droit de modérer tout message hors sujet (politique, sanitaire...), non lié au technique, irrespectueux ou illégal.

🤖 Voici quelques commandes utiles à indiquer dans un message puis à envoyer :
/admin permet d'identifier les administrateurs
/rules vous affiche le règlement du groupe
/link indique le lien d'invitation du groupe

⁉️ Une liste de mots interdits a été rédigée pour limiter le spam, le phishing ou encore la confidentialité dont voici un échantillon : pass, fb.com, facebook.com, vaccination, docteur, yescard, carte carburant...

💡 Avant de demander, faites une recherche sur le groupe (avec la 🔍 dans la barre en haut) avec le mot clé correspondant à votre demande. Vous trouverez sûrement des dizaines de réponses à ce sujet !

---
🐧 Le lien du wiki :
<a href="https://wikilibriste.fr">Wikilibriste - Wiki</a>
🐧 Le lien du groupe Telegram :
<a href="https://t.me/securite_informatique_libre">Wikilibriste : Numérique Éthique et Résilient</a>
🐧 Canal d'information lié aux outils :
<a href="https://t.me/outils_libres">Outils libres</a>

https://t.me/outils_libres

ℹ️ D'autres groupes permettent d'échanger sur d'autres sujets :
<a href="https://t.me/partage_citoyen_france">Infos citoyennes France</a>

🍁 <b>L'équipe d'administrateurs se fera un plaisir de répondre à toute demande.</b>

Merci à tous.
"""
GAFAM_RULES="""
✅ <b>Notre groupe traite toutes les questions relatives aux outils sous licences Libre ou à minima Open Source, et respectueux de votre vie privée.</b>
Voir <a href="https://wikilibriste.fr/fr/glossaire#open-source">Libre/Open Source</a>.

🛑  <b>Notre groupe ne traite pas des outils privateurs et en particulier ceux liés aux <a href="https://wikilibriste.fr/fr/glossaire#gafam">GAFAM/BATX</a>.</b>
L'éthique de ces entreprises ne correspond pas à nos valeurs!

👉 Si vous êtes intéressés pour vous libérer, n'hésitez pas à poser vos questions ici.
👉 Sinon, vous pouvez vous diriger vers d'autres canaux d'information ou de forums.

💡 N'hésitez pas à faire une recherche sur le groupe (avec la 🔍 dans la barre en haut) avec le mot clé correspondant à votre demande avant de poser votre question.
Vous trouverez sûrement des dizaines de réponses !

🍁 <b>L'équipe d'administrateurs se fera un plaisir de répondre à toute demande.</b>
"""
MSG_DON = """
✅ <b>Voici toutes les informations pour nous faire un don.</b>

Monnaies acceptées :
- Euros
- June
- Bitcoin (BTC - <b>A venir</b>)
- Monero (XMR - <b>A venir</b>)

Il est possible également d'effectuer des échanges de services.
💡 N'hésitez pas à contacter les administrateurs.


🐧 Le lien de la page du wiki :
<a href="https://wikilibriste.fr/fr/contribution">Wikilibriste - Soutien</a>
🐧 Le lien de la page Open Collective (don en Euros):
<a href="https://opencollective.com/wikilibriste?language=fr">Wikilibriste - Open Collective</a>
🐧 Le lien de la page Liberapay (don en Euros):
<a href="https://fr.liberapay.com/wikilibriste/">Wikilibriste - Liberapay</a>

🍁 <b>L'équipe vous remercie infiniment.</b>
"""
MSG_CRYPT = """
💡 Petit point vocabulaire : <b>Crypter ou Chiffrer ?</b>

Comme tout le monde le sait, nous sommes tatillons, mais les mots ont leur importance à nos yeux car ils véhiculent une idée ou un concept précis : crypter vs chiffrer.
La terminologie crypter ne doit pas être utilisée, ou dans de rares occasions ; à moins de faire un anglicisme, qui dévoie le sens initial des termes FR : 
👉 crypter = "coder", au sens protéger, sans produire la clé pour "décoder" (le mot de passe de déchiffrement ici), ce qui serait peu pratique !
et
👉 décrypter = analyser en vue de casser un code / deviner et recomposer un message chiffré (ce qui est mission impossible pour un humain de nos jours avec les algorithmes de chiffrement, combinés à la puissance de calcul de l'informatique moderne).

👉 chiffrer = action de protéger une information pour le rendre illisible à ceux qui n'ont pas le secret.
et
👉 déchiffrer = retrouver le contenu de l'information préalablement chiffrée, à l'aide de la clé de déchiffrement. 


Un site a même été créé à l'occasion, tant cet abus de langage est courant :
https://chiffrer.info/
"""
MSG_FILE = """
💡 Règles du groupe :
<b>Merci de ne pas partager de fichiers sur le groupe (.apk, .exe, .jar, .zip, .csv, .xlsx...)</b>

👉 Il est préférable d'envoyer le lien vers le dépôt ou le code source directement.

👉 Il est recommandé de télécharger les fichiers APK sur F-droid (ou équivalent) ou Aurora store.

🛑 Et pour nos malheureux membres utilisant toujours Windows : de façon générale, toujours faire attention aux .exe ou .msi provenant de sources hors de l'éditeur de logiciel (bien souvent source de malware pour vol de données ou pire) !
"""
